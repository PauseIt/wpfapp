﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatingApp.Lib;

namespace DatingApp.ViewModel
{
    class ShowProfileViewModel : BaseViewModel, IPageViewModel
    {

        public string UserName { get; set; }
        public int UserId { get; set; }

        public ShowProfileViewModel()
        {
            this.UserId = ThisShowProfile.User.id;
            this.UserName = ThisShowProfile.User.user_name;
        }
    }
}
