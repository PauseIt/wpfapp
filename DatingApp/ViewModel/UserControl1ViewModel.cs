﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DatingApp.ViewModel
{
    class UserControl1ViewModel : BaseViewModel, IPageViewModel
    {
        DataClasses1DataContext context = new DataClasses1DataContext();
        private ICommand _addUserBtn;

        private string _user_email;
        private string _user_password;

        public string user_email
        {
            get { return this._user_email;  }
            set { this._user_email = value;  }
        }
        public string user_password
        {
            get { return this._user_password; }
            set { this._user_password = value; }
        }
        public string UserEmail
        {
            get {
                return this._user_email;
            }
            set
            {
                this._user_email = value;
            }
        }

        private void TryLogin( object obj = null )
        {
            user LoginUser = (from u in context.users where u.email == this.user_email && u.password == this.user_password select u).SingleOrDefault();
            if (LoginUser != null && LoginUser.id > 0)
            {
                DatingApp.Lib.ThisUser.User = LoginUser;
            }
            Mediator.Notify("Login", "");
        }


        private ICommand _loginBtn;
        public ICommand LoginBtn
        {
            get
            {
                
                return _loginBtn ?? (_loginBtn = new RelayCommand(TryLogin));
            }
        }

        public ICommand AddUserBtn
        {
            get
            {
                return _addUserBtn ?? (_addUserBtn = new RelayCommand(x =>
                {
                    // does email exist?
                    Mediator.Notify("AddUser", UserEmail);
                }));
            }
        }
    }
}
