﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatingApp.View
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : UserControl
    {
        DataClasses1DataContext context = new DataClasses1DataContext();
        //user User = DatingApp.Lib.ThisUser.Instance;

        public AddUser()
        {
            InitializeComponent();

            var eye_colours = from c in context.eye_colours select c;
            eye_colours_box.DataContext = eye_colours;
            InitializeComponent();
            var hair_colours = from c in context.hair_colours select c;
            hair_colours_box.DataContext = hair_colours;
            InitializeComponent();
            var body_types = from c in context.body_types select c;
            body_types_box.DataContext = body_types;


        }

        private void Body_types_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dynamic selType = body_types_box.SelectedItem;
            DatingApp.Lib.ThisUser.User.body_type_id = selType.id;
        }

        private void Eye_colours_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dynamic selColour = eye_colours_box.SelectedItem;
            DatingApp.Lib.ThisUser.User.eye_colour_id = selColour.id;
        }

        private void Hair_colours_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dynamic selColour = hair_colours_box.SelectedItem;
            DatingApp.Lib.ThisUser.User.hair_colour_id = selColour.id;
        }
    }
}
