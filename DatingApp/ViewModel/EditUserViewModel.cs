﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DatingApp.Lib;

namespace DatingApp.ViewModel
{
    
    class EditUserViewModel : BaseViewModel, IPageViewModel
    {
        DataClasses1DataContext context = new DataClasses1DataContext();
        user MyUser
        {
            get { return ThisUser.User; }
            set { ThisUser.User = value; }
        }

        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string UserDescription { get; set; }
        public DateTime BirthDate { get; set; }
        public Decimal Height { get; set; }
        public Decimal Weight { get; set; }

        public EditUserViewModel ()
        {
            user User = DatingApp.Lib.ThisUser.User;
            Email = User.email;
            Firstname = User.firstname;
            Password = User.password;
            UserName = User.user_name;
            UserDescription = User.user_description;
            BirthDate = User.birth_date;
            Height = User.height;
            Weight = User.weight;
        }

        private ICommand _editUser;
        
        public ICommand EditUser
        {
            get
            {
                return _editUser ?? (_editUser = new RelayCommand(tryUpdateUser));
            }
        }

        public void tryUpdateUser( object obj = null ) {

            user new_user = this.MyUser;
            new_user.email = Email;
            new_user.firstname = Firstname;
            new_user.password = Password;
            new_user.user_name = UserName;
            new_user.user_description = UserDescription;
            new_user.birth_date = BirthDate;
            new_user.height = Height;
            new_user.weight = Weight;


            var query_user = context.users.Where(Usr => Usr.id == MyUser.id).FirstOrDefault();

            query_user.email = Email;
            query_user.firstname = Firstname;
            query_user.password = Password;
            query_user.user_name = UserName;
            query_user.user_description = UserDescription;
            query_user.birth_date = BirthDate;
            query_user.height = Height;
            query_user.weight = Weight;

            //context.SubmitChanges();


            //context.users.(new_user);

            // Submit the change to the database.
            try
            {
                context.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("User not updated");

            }
            finally
            {
                if (new_user.id > 0)
                {
                    MessageBox.Show("user inserted");
                    DatingApp.Lib.ThisUser.User = new_user;
                }
                else
                {
                    MessageBox.Show("user not inserted");
                }

            }
            Mediator.Notify("Dashboard", new_user);
        }


        

    }
}
