﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatingApp.Lib;

namespace DatingApp.View
{
    class MatchForXaml
    {
        public int MatchId { get; set; }
        public int MatchUserId { get; set; }
        public user MatchUser { get; set; }
        public string MatchUserName { get; set; }

        private ICommand _chatBtn;
        public ICommand ChatBtn
        {
            get
            {
                return this._chatBtn ?? (this._chatBtn = new RelayCommand(x => {
                    Mediator.Notify("Chat", this.MatchUserId);
                }));
            }
        }
    }
    /// <summary>
    /// Interaction logic for Matches.xaml
    /// </summary>
    public partial class Matches : UserControl
    {
        private readonly DataClasses1DataContext context = new DataClasses1DataContext();
        public string TESTVAR { get; set; }
        IEnumerable<match> UserMatches;
        List<MatchForXaml> _matchForXaml { get => matchForXaml; set => matchForXaml = value; }
        List<MatchForXaml> MatchesForXaml { get { return _matchForXaml; } set { _matchForXaml = value; } }

        public Matches()
        {

            InitializeComponent();
            this._matchForXaml = new List<MatchForXaml>();
            this.TESTVAR = "DETTE ER MATCHES!!!!!";
            this.UserMatches = from m in context.matches join um in context.user_matches on m.id equals um.match_id where um.user_id == ThisUser.User.id && m.is_active select m;


            foreach (match m in UserMatches)
            {
                int match_user_id = m.user_matches.FirstOrDefault().match_id;
                int match_id = m.id;
                user match_user = (from u in context.users where u.id == match_user_id select u).SingleOrDefault();
                string match_user_name = match_user.user_name;

                MatchForXaml t_MatchForXaml = new MatchForXaml();
                t_MatchForXaml.MatchId = match_id;
                t_MatchForXaml.MatchUserId = match_user_id;
                t_MatchForXaml.MatchUser = match_user;
                t_MatchForXaml.MatchUserName = match_user_name;
                MatchesForXaml.Add(t_MatchForXaml);
            }


            MatchesForXamlView.DataContext = MatchesForXaml;
        }
        private List<MatchForXaml> matchForXaml;
    }

}
