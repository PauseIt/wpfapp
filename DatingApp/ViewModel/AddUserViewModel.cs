﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace DatingApp.ViewModel
{
    class AddUserViewModel : BaseViewModel, IPageViewModel {
        DataClasses1DataContext context = new DataClasses1DataContext();

        private ICommand _insertUser;


        public void tryInsertUser(object obj = null)
        {
            user NewUser = new user();
            NewUser.email = this.email;
            NewUser.firstname = this.firstname;
            NewUser.password = this.password1;
            NewUser.user_name = this.user_name;
            NewUser.birth_date = this.birth_date;
            NewUser.height = this.height;
            NewUser.weight = this.weight;
            NewUser.hair_colour_id = this.hair_colour;
            NewUser.eye_colour_id = this.eye_colour;
            NewUser.body_type_id = this.body_type;
            NewUser.gender = this.gender;
            NewUser.created_datetime = DateTime.Now;
            NewUser.is_active = true;

            context.users.InsertOnSubmit(NewUser);

            // Submit the change to the database.
            try
            {
                context.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("not que");

            }
            finally
            {
                if (NewUser.id > 0)
                {
                    MessageBox.Show("user inserted");
                    DatingApp.Lib.ThisUser.User = NewUser;
                }
                else
                {
                    MessageBox.Show("user not inserted");
                }

            }
            Mediator.Notify("Dashboard", this.User);
        }


        public ICommand InsertUser
        {
            get
            {
                return _insertUser ?? (_insertUser = new RelayCommand(tryInsertUser, Validate));
            }
        }

        user User = DatingApp.Lib.ThisUser.Instance;
        public string email { get; set; }
        public string firstname { get; set; }
        public string password1 { get; set; }
        public string password2 { get; set; }
        private string _user_name = "";
        public string user_name {
            get { return this._user_name ?? "";  }
            set
            {
                this._user_name = ( !this.user_name_exist(value) ) ? value.Trim() : "";
            }
        }
        public DateTime birth_date { get; set; }
        public decimal height { get; set; }
        public decimal weight { get; set; }
        public int hair_colour { get; set; }
        public int eye_colour { get; set; }
        public int body_type { get; set; }
        public Boolean gender { get; set; }

        

        public AddUserViewModel()
        {
            DatingApp.Lib.ThisUser.User = new user();

            this.email = this.User.email?? "";



            this.password1 = "";
            this.password2 = "";
            this.eye_colour = 0;
            
        }
       
        public bool Validate( object obj = null)
        {
            bool is_validate = true;
            this.eye_colour = DatingApp.Lib.ThisUser.User.eye_colour_id ?? 0;
            this.hair_colour = DatingApp.Lib.ThisUser.User.hair_colour_id ?? 0;
            this.body_type = DatingApp.Lib.ThisUser.User.body_type_id ?? 0;

            if (password1.Trim().Length < 2 || password1 != password2)
            {
                is_validate = false;
            }
            if (user_name.Trim().Length < 5)
            {
                is_validate = false;
            }

            if (is_validate)
            {
                
                if(this.user_name_exist(this.user_name ?? ""))
                {
                    is_validate = false;
                }
            }

            if (this.eye_colour < 1 )
            {
                is_validate = false;
            }
            if (this.email.Trim().Length < 10)
            {
                is_validate = false;
            }

            return is_validate;
        }

        private bool user_name_exist(string user_name = "")
        {
            return user_name.Trim().Length < 5 || (from c in context.users where c.user_name == user_name select c.id).SingleOrDefault() > 0;
        }

        
    }
}
