﻿using DatingApp.Lib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace DatingApp.ViewModel
{
    
    class DashboardViewModel : BaseViewModel, IPageViewModel
    {
        user User;

        private DataClasses1DataContext context;

        private ObservableCollection<message> _messages;
        public ObservableCollection<message> Messages
        {
            get
            {
                return _messages;
            }
            set
            {
                this._messages = value;
                base.OnPropertyChanged("Messages");
            }
        }

        private ObservableCollection<DashboardMessage> _dashboardMessages;
        public ObservableCollection<DashboardMessage> DashboardMessages
        {
            get
            {
                return _dashboardMessages;
            }
            set
            {
                this._dashboardMessages = value;
                base.OnPropertyChanged("Messages");
            }
        }

        private ObservableCollection<match> _matches;
        public ObservableCollection<match> Matches
        {
            get
            {
                return _matches;
            }
            set
            {
                this._matches = value;
                base.OnPropertyChanged("Matches");
            }
        }

        private ObservableCollection<DashboardMatch> _dashboardMatches;
        public ObservableCollection<DashboardMatch> DashboardMatches
        {
            get
            {
                return _dashboardMatches;
            }
            set
            {
                this._dashboardMatches = value;
                base.OnPropertyChanged("DashboardMatches");
            }
        }

        public DashboardViewModel()
        {
            this.User = ThisUser.User;
            this.context = new DataClasses1DataContext();

            this.Messages = GetMessages();
            this.DashboardMessages = GetDashboardMessages();
            this.Matches = GetMatches();
            this.DashboardMatches = GetDashboardMatches();

        }

        public static ObservableCollection<message> GetMessages()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var messages = new ObservableCollection<message>();
            int match_id = ThisMatch.Match.id;

            IEnumerable<message> new_messages =
                from message in context.messages
                    join match in context.matches on message.match_id equals match.id
                    join user_match in context.user_matches on match.id equals user_match.match_id
                where user_match.user_id == ThisUser.User.id
                    && message.user_id != ThisUser.User.id
                select message;
            foreach (message m in new_messages) {
                messages.Add(m);
            }
            return messages;
        }

        public static ObservableCollection<DashboardMessage> GetDashboardMessages()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var DashboardMessages = new ObservableCollection<DashboardMessage>();
            int match_id = ThisMatch.Match.id;

            IEnumerable<message> new_messages =
                from message in context.messages
                join match in context.matches on message.match_id equals match.id
                join user_match in context.user_matches on match.id equals user_match.match_id
                where user_match.user_id == ThisUser.User.id
                    && message.user_id != ThisUser.User.id
                select message;
            foreach ( message m in new_messages ) {
                DashboardMessages.Add(new DashboardMessage(m));
            }
            return DashboardMessages;
        }

        public static ObservableCollection<match> GetMatches()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var matches = new ObservableCollection<match>();
            //int match_id = ThisMatch.Match.id;

            IEnumerable<match> new_matches =
                from match in context.matches
                    join match_user in context.user_matches on match.id equals match_user.match_id
                    join user_match in context.user_matches on match.id equals user_match.match_id
                where user_match.user_id == ThisUser.User.id
                    && match_user.user_id != ThisUser.User.id
                select match;
            
            foreach (match m in new_matches) {
                matches.Add(m);
            }
            return matches;

        }

        public static ObservableCollection<DashboardMatch> GetDashboardMatches()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var DashboardMatchs = new ObservableCollection<DashboardMatch>();
            //int match_id = ThisMatch.Match.id;

            IEnumerable<match> new_matches =
                from match in context.matches
                join match_user in context.user_matches on match.id equals match_user.match_id
                join user_match in context.user_matches on match.id equals user_match.match_id
                where user_match.user_id == ThisUser.User.id
                    && match_user.user_id != ThisUser.User.id
                select match;

            foreach (match m in new_matches) {
                DashboardMatchs.Add(new DashboardMatch(m));
            }
            return DashboardMatchs;
        }


    }

    class DashboardMessage {
        DataClasses1DataContext context = new DataClasses1DataContext();
        public int UserId { get; set; }
        public string Message { get; set; }
        public int MatchId { get; set; }
        public string UserName { get; set; }

        public DashboardMessage() { }

        public DashboardMessage( message Message ) {
            this.UserId = Message.user_id;
            this.Message = Message.message1;
            this.MatchId = Message.match_id;
            this.UserName = (from u in context.users where u.id == UserId select u.user_name).SingleOrDefault();
        }

        private ICommand _chatBtn;
        public ICommand ChatBtn
        {
            get
            {
                return this._chatBtn ?? (this._chatBtn = new RelayCommand(x => {
                    Mediator.Notify("Chat", this.MatchId);
                }));
            }
        }

        public override string ToString() {
            return this.UserName + ": " + this.Message;
        }
    }

    class DashboardMatch {
        DataClasses1DataContext context = new DataClasses1DataContext();
        public string UserName { get; set; }
        public int UserId { get; set; }
        public int MatchId { get; set; }
        public match Match { get; set; }
        public user User { get; set; }

        public DashboardMatch( match Match ) {
            this.Match = Match;
            this.MatchId = Match.id;
            this.User = (from um in context.user_matches where um.match_id == this.Match.id && um.user_id != ThisUser.User.id select um.user).SingleOrDefault();
            this.UserName = this.User.user_name;
            this.UserId = this.User.id;
        }

        private ICommand _chatBtn;
        public ICommand ChatBtn
        {
            get
            {
                return this._chatBtn ?? (this._chatBtn = new RelayCommand(x => {
                    Mediator.Notify("Chat", this.MatchId);
                }));
            }
        }

        private ICommand _showProfileBtn;
        public ICommand ShowProfileBtn
        {
            get
            {
                return this._showProfileBtn ?? (this._showProfileBtn = new RelayCommand(x => {
                    Mediator.Notify("ShowProfile", this.UserId);
                }));
            }
        }

        public override string ToString() {
            return User.ToString();
        }
    }
}
