﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DatingApp.ViewModel
{
    class MainWindowViewModel : BaseViewModel, IPageViewModel
    {
        user User = DatingApp.Lib.ThisUser.Instance;
        DataClasses1DataContext context = new DataClasses1DataContext();

        string user_email = "";
        string user_password = "";


        private ICommand _loginBtn;
        public ICommand LoginBtn
        {
            get
            {
                DatingApp.Lib.ThisUser.User = (from u in context.users where u.email == this.user_email && u.password == this.user_password select u).SingleOrDefault();
                return _loginBtn ?? (_loginBtn = new RelayCommand(x => {
                    Mediator.Notify("Dashboard", "");
                }));
            }
        }
        private ICommand _dashboardBtn;
        public ICommand DashboardBtn {
            get { return _dashboardBtn ?? (_dashboardBtn = new RelayCommand(x => {
                    Mediator.Notify("Dashboard", ""); }));
            }
        }
        private ICommand _searchBtn;
        public ICommand SearchBtn
        {
            get
            {
                return _searchBtn ?? (_searchBtn = new RelayCommand(x => {
                    Mediator.Notify("Search", "");
                }));
            }
        }
        private ICommand _editUserBtn;
        public ICommand EditUserBtn
        {
            get
            {
                return _editUserBtn ?? (_editUserBtn = new RelayCommand(x => {
                    Mediator.Notify("EditUser", "");
                }));
            }
        }
        private ICommand _matchesBtn;
        public ICommand MatchesBtn
        {
            get
            {
                return _matchesBtn ?? (_matchesBtn = new RelayCommand(x => {
                    Mediator.Notify("Matches", "");
                }));
            }
        }
        
        private ICommand _logoutBtn;
        public ICommand LogoutBtn
        {
            get
            {
                DatingApp.Lib.ThisUser.User = null;
                DatingApp.Lib.ThisUser.User = new user();
                return _logoutBtn ?? (_logoutBtn = new RelayCommand(x => {
                    Mediator.Notify("Logout", "");
                }));
            }
        }

        


        private IPageViewModel _currentPageViewModel;
        private List<IPageViewModel> _pageViewModels;



        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (_pageViewModels == null)
                    _pageViewModels = new List<IPageViewModel>();

                return _pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return _currentPageViewModel;
            }
            set
            {
                _currentPageViewModel = value;
                OnPropertyChanged("CurrentPageViewModel");
            }
        }

        private void ChangeViewModel(IPageViewModel viewModel)
        {

            if ( !( viewModel is AddUserViewModel ) && DatingApp.Lib.ThisUser.User.id < 1 )
            {
                viewModel = PageViewModels[0];
            }
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);

            CurrentPageViewModel = PageViewModels
                .FirstOrDefault(vm => vm == viewModel);
        }

        private void OnGo1Screen(object obj)
        {
            ChangeViewModel(PageViewModels[0]);
        }

        private void OnGo2Screen(object obj)
        {
            ChangeViewModel(PageViewModels[1]);
        }

        private void AddUser(object obj)
        {
            User.email = (string)obj;
            ChangeViewModel(PageViewModels[2]);
        }

        private void Dashboard(object obj)
        {
            ChangeViewModel(PageViewModels[3]);
        }

        private void Login( object obj = null )
        {
            if (DatingApp.Lib.ThisUser.User.id < 1)
            {
                TryLogin();
            }
            ChangeViewModel(PageViewModels[3]);
        } 
        private void TryLogin()
        {
            user LoginUser = (from u in context.users where u.email == this.user_email && u.password == this.user_password select u).SingleOrDefault();
            if (LoginUser != null && LoginUser.id > 0) {
                DatingApp.Lib.ThisUser.User = LoginUser;
            }
        }

        private void Search(object obj)
        {
            ChangeViewModel(PageViewModels[4]);
        }
        private void EditUser(object obj)
        {
            ChangeViewModel(PageViewModels[5]);
        }
        private void Matches(object obj)
        {
            ChangeViewModel(PageViewModels[6]);
        }
        private void Chat(object obj)
        {
            int ThisMatchId = (int)obj;
            //MessageBox.Show("trying to open chat: " + ThisMatchId);
            match ThisMatch = ( from m in context.matches where m.id == ThisMatchId select m ).SingleOrDefault();
            DatingApp.Lib.ThisMatch.Match = ThisMatch;
            ChangeViewModel(PageViewModels[7]);
        }
        private void Logout(object obj)
        {
            ChangeViewModel(PageViewModels[0]);
        }
        private void ShowProfile(object obj)
        {
            int ShowProfileUserId = (int)obj;
            DatingApp.Lib.ThisShowProfile.User = (from u in context.users where u.id == ShowProfileUserId select u).SingleOrDefault();
            ChangeViewModel(PageViewModels[8]);
        }


        public MainWindowViewModel()
        {
            // Add available pages and set page
            PageViewModels.Add(new UserControl1ViewModel());
            PageViewModels.Add(new UserControl2ViewModel());
            PageViewModels.Add(new AddUserViewModel());
            PageViewModels.Add(new DashboardViewModel());

            PageViewModels.Add(new SearchViewModel());
            PageViewModels.Add(new EditUserViewModel());
            PageViewModels.Add(new MatchesViewModel());
            PageViewModels.Add(new ChatViewModel());

            PageViewModels.Add(new ShowProfileViewModel());
            //PageViewModels.Add(new UserControl1ViewModel());

            CurrentPageViewModel = PageViewModels[0];

            Mediator.Subscribe("GoTo1Screen", OnGo1Screen);
            Mediator.Subscribe("GoTo2Screen", OnGo2Screen);
            Mediator.Subscribe("AddUser", AddUser);
            Mediator.Subscribe("Dashboard", Dashboard);
            Mediator.Subscribe("Login", Login);

            Mediator.Subscribe("Search", Search);
            Mediator.Subscribe("EditUser", EditUser);
            Mediator.Subscribe("Matches", Matches);
            Mediator.Subscribe("Chat", Chat);
            Mediator.Subscribe("Logout", Logout);
            Mediator.Subscribe("ShowProfile", ShowProfile);
        }
    }
}
