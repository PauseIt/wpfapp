﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DatingApp.Lib;

namespace DatingApp.ViewModel
{
    class SearchViewModel : BaseViewModel, IPageViewModel
    {
        public DataClasses1DataContext context = new DataClasses1DataContext();

        private RandomUser _randomUser;
        public RandomUser RandomUser { get { return this._randomUser; } set { this._randomUser = value; base.OnPropertyChanged("RandomUser"); } }


        

        public SearchViewModel ()
        {
            FindRandomUser();
        }

        public void FindRandomUser(){
            //user NewUser = (from u in context.users where u.id != ThisUser.User.id select u).Skip(new Random().Next()).SingleOrDefault();
            user NewUser = (from u in context.users where u.id != ThisUser.User.id select u).FirstOrDefault();
            RandomUser RandomUser = new RandomUser(NewUser, this);
            this.RandomUser = RandomUser;
        }

        internal void SaveSwipe(swipe swipe)
        {


            //check efter march

            

            swipe other_swipe = (from s in context.swipes where s.target_user_id == swipe.user_id && s.user_id == swipe.target_user_id select s).SingleOrDefault();
            bool other_is_like = (other_swipe != null) ? other_swipe.is_like : false;

            if (swipe.is_like && other_is_like)
            {
                context.swipes.DeleteOnSubmit(other_swipe);
                //slet other swipe
                match NewMatch = new match();
                
                
                //new march
                //insert march
                //alert box 
            }
            else
            {
                // gem swipe
            }




















            if (swipe.is_like)
            {
                
                
            }
            
            if (other_is_like == true)
            {

                //slet swipe
                //insert march
                //alert box march
            }
            else
            {
                //gem et swipe i databasen
            }








        }
    }

    class RandomUser
    {

        public string UserName { get; set; }
        public int UserId { get; set; }
        public user User { get; set; }


        public RandomUser(user User, SearchViewModel viewModel)
        {
            this.ViewModel = viewModel;
            DataClasses1DataContext context = new DataClasses1DataContext();
            this.User = User;
            this.UserName = this.User.user_name;
            this.UserId = this.User.id;
        }

        private ICommand _showprofilebtn;
        public ICommand ShowProfileBtn
        {
            get
            {
                return this._showprofilebtn ?? (this._showprofilebtn = new RelayCommand(x => {
                    Mediator.Notify("ShowProfile", this.UserId);
                }));
            }
        }
        private ICommand _likebtn;
        public ICommand LikeBtn
        {
            get
            {
                return this._likebtn ?? (new DoSwipe(this, this.ViewModel, true, false));
            }
        }

        

        private ICommand _superlikebtn;
        public ICommand SuperLikeBtn
        {
            get
            {
                return this._superlikebtn ?? (new DoSwipe(this, this.ViewModel, true, true));
            }
        }
        private ICommand _notlikebtn;
        private SearchViewModel ViewModel;

        public ICommand NotLikeBtn
        {
            get
            {
                return this._notlikebtn ?? (new DoSwipe(this, this.ViewModel, false, false));
            }
        }

    }
}
