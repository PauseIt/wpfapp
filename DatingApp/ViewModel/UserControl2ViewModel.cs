﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DatingApp.ViewModel
{
    class UserControl2ViewModel : BaseViewModel, IPageViewModel
    {
        //DataClasses1DataContext context = new DataClasses1DataContext();

        public UserControl2ViewModel() {
            //var swipes = from s in context.swipes where s.id > 0 select s;
        }

        private ICommand _goTo1;

        public ICommand GoTo1
        {
            get
            {
                return _goTo1 ?? (_goTo1 = new RelayCommand(x =>
                {
                    Mediator.Notify("GoTo1Screen", "");
                }));
            }
        }
    }
}
