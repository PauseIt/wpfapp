﻿using DatingApp.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace DatingApp
{

    public partial class match
    {
        DataClasses1DataContext context = new DataClasses1DataContext();
        public override string ToString()
        {
            string String = "";
            int match_id = this.id;
            int match_user_id = (from m in context.user_matches where m.match_id == match_id && m.user_id != ThisUser.User.id select m.user_id).FirstOrDefault();
            user match_user = (from u in context.users where u.id == match_user_id select u).FirstOrDefault();
            String = match_user.ToString();
            return String;
        }
    }

    public partial class eye_colour
    {
        public override string ToString()
        {
            string String = this.colour;
            return String;
        }
    }

    public partial class interest
    {
        public override string ToString()
        {
            string String = this.name;
            return String;
        }
    }

    public partial class interest_group
    {
        public override string ToString()
        {
            string String = this.name;
            return String;
        }
    }

    public partial class body_type
    {
        public override string ToString()
        {
            string String = this.type;
            return String;
        }
    }

    public partial class message
    {
        public override string ToString()
        {
            string String = "";
            if ( this.id > 0 )
            {
                if (this.user == null)
                {
                    DataClasses1DataContext context = new DataClasses1DataContext();
                    this.user = (from u in context.users where u.id == this.user_id select u).SingleOrDefault();
                }
                String = this.user.user_name + ": " + this.message1;
            }
            else
            {
                String = "No user found : no message found";
            }
           
            return String;
        }
    }

    public partial class hair_colour
    {
        public override string ToString()
        {
            string String = this.colour;
            return String;
        }
    }

    public partial class user_file
    {
        public override string ToString()
        {
            string String = this.file.data.ToString();
            return String;
        }
    }

    public partial class message_file
    {
        public override string ToString()
        {
            string String = this.file.data.ToString();
            return String;
        }
    }

    public partial class file
    {
        public void getit()
        {
            //this.data;
            //string String = Convert.FromBase64String(this.data.ToString());
        }
        
    }

    public partial class user
    {
        public override string ToString()
        {
            string String = this.user_name;
            return String;
        }
    }

}
