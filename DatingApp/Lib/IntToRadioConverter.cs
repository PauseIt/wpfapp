﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DatingApp.Lib
{
    [ValueConversion(typeof(int), typeof(bool))]
    public class IntToRadioConverter : IValueConverter
    {
        public object Convert(object value, Type t, object parameter, CultureInfo culture)
        {
            return System.Convert.ToInt32(parameter) > 0;

            //return System.Convert.ToInt32(value) == System.Convert.ToInt32(parameter);
        }

        public object ConvertBack(object value, Type t, object parameter, CultureInfo culture)
        {
            return System.Convert.ToBoolean(value) ? 1 : 0;

            //return value.Equals(false) ? DependencyProperty.UnsetValue : parameter;
        }
        
    }
}
