﻿using System;
using System.Windows.Input;
using DatingApp.Lib;

namespace DatingApp.ViewModel
{
    internal class DoSwipe : ICommand
    {
        private RandomUser randomUser;
        private SearchViewModel ViewModel;
        private bool IsLike;
        private bool IsSuperLike;
        public DataClasses1DataContext context = new DataClasses1DataContext();

        public DoSwipe(RandomUser randomUser, SearchViewModel viewModel, bool is_like, bool is_super_like)
        {
            this.randomUser = randomUser;
            this.ViewModel = viewModel;
            this.IsLike = is_like;
            this.IsSuperLike = is_super_like;

        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            
        swipe Swipe = new swipe();
            Swipe.user_id = ThisUser.User.id;
            Swipe.target_user_id = randomUser.UserId;
            Swipe.is_like = this.IsLike;
            Swipe.is_super_like = this.IsSuperLike;
            Swipe.datetime = DateTime.Now;

            //exec query
            ViewModel.SaveSwipe(Swipe);

            //check for match
            

            ViewModel.FindRandomUser();
        }
    }
}