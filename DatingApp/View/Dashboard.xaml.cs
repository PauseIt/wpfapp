﻿using System.Windows.Controls;

namespace DatingApp.View
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        public Dashboard()
        {
            InitializeComponent();
            DataContext = new DatingApp.ViewModel.DashboardViewModel();
        }
    }
}
