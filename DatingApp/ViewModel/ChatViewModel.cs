﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DatingApp.Lib;

namespace DatingApp.ViewModel
{
    class ChatViewModel : BaseViewModel, IPageViewModel
    {
        public DataClasses1DataContext context = new DataClasses1DataContext();

        private ObservableCollection<message> _messages;
        public ObservableCollection<message> Messages
        {
            get
            {
                return _messages;
            }
            set
            {
                this._messages = value;
                base.OnPropertyChanged("Messages");
            }
        }

        private ObservableCollection<ChatMessage> _chatMessages;
        public ObservableCollection<ChatMessage> ChatMessages
        {
            get
            {
                return _chatMessages;
            }
            set
            {
                this._chatMessages = value;
                base.OnPropertyChanged("ChatMessages");
            }
        }

        private string _newMessage;
        public string NewMessage
        {
            get { return _newMessage; }
            set {
                this._newMessage = value;
                base.OnPropertyChanged("NewMessage" );
            }
        }

        


        private ICommand _addNewMessage;
        public ICommand AddNewMessage
        {
            get
            {
                if (this._addNewMessage == null)
                {
                    this._addNewMessage = new AddNewMessage(this);
                }
                return _addNewMessage;
            }
            
        }

       

        public ChatViewModel()
        {
            this._messages = GetMessages();
            this.ChatMessages = GetChatMessages();
        }

        public static ObservableCollection<message> GetMessages()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var messages = new ObservableCollection<message>();
            int match_id = ThisMatch.Match.id;

            IEnumerable<message> new_messages = (from m in context.messages where m.match_id == match_id select m);
            foreach (message m in new_messages)
            {
                messages.Add(m);

            }
            return messages;
        }

        public static ObservableCollection<ChatMessage> GetChatMessages()
        {
            DataClasses1DataContext context = new DataClasses1DataContext();
            var ChatMessages = new ObservableCollection<ChatMessage>();
            int match_id = ThisMatch.Match.id;

            IEnumerable<message> new_messages = (from m in context.messages where m.match_id == match_id select m);
            foreach (message m in new_messages)
            {
                ChatMessages.Add(new ChatMessage(m));

            }
            return ChatMessages;
        }
    }

    class ChatMessage
    {
        DataClasses1DataContext context = new DataClasses1DataContext();

        public message Message { get; set; }
        public user User { get; set; }

        public ChatMessage ( message Message ) {
            this.Message = Message;
            this.User = (from u in context.users where u.id == this.Message.user_id select u).SingleOrDefault();
            this.Begin();
        }

        private void Begin() {

        }

        public override string ToString() {
            if ( this.Message != null )
            {
                return this.Message.ToString();
                //return this.User.ToString() + ": " + this.Message.ToString();
            }
            else
            {
                return "No message";
            }
        }

        
    }


    class AddNewMessage : ICommand
    {
        public ChatViewModel ViewModel;
        public AddNewMessage(ChatViewModel ViewModel)
        {
            this.ViewModel = ViewModel;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            message new_message = new message();
            new_message.user_id = ThisUser.User.id;
            new_message.match_id = ThisMatch.Match.id;
            new_message.message1 = ViewModel.NewMessage;
            new_message.datetime = DateTime.Now;
            new_message.is_reported = false;
            new_message.is_seen = false;

            ViewModel.ChatMessages.Add(new ChatMessage(new_message));
            ViewModel.NewMessage = "";

            ViewModel.context.messages.InsertOnSubmit(new_message);

            // Submit the change to the database.
            try
            {
                ViewModel.context.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("not que" + e.ToString());

            }
            finally
            {
                if (new_message.id > 0)
                {
                    MessageBox.Show("message is saved");
                }
                else
                {
                    MessageBox.Show("message not inserted");
                }

            }
        }
    }
}
